{*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Benoit MOTTIN <benaesan@msn.com>
*  @copyright  LocalAddict
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}



	<!-- 
		TODO
		Mettre en forme le formulaire
		mettre le HTML en responsive
	 -->

<!--	 
	<img class="img-responsive" id="banner_back" src="{$banner_img}" alt="banniere" title="Banniere SurLeCHamp"/>


<div class="slc_40">	
<a href="{$base_dir}" title="Banniere SurLeChamp" >
	<img class="img-responsive " id="banner_logo" src="{$logo_img}" alt="logo surlechamp" title="logo SurLeCHamp"/>
	<img class="img-responsive " src="{$baseline_img}" alt="baseline surlechamp" title="baseline SurLeCHamp"/>
</a>
</div>
{* si le client est logger, le formulaire n'est pas affiché *}
<img class="" id="english_flag_back" src="{$english_flag_img}" alt="Drapeau Anglais"/>
<img class="" id="french_flag_back" src="{$french_flag_img}" alt="Drapeau Francais"/>
{if !$logged}
	{if $page_name != 'authentication'}
	<form action="{$link->getPageLink('authentication', true)|escape:'html':'UTF-8'}" method="post" id="login_form" class="box">
		<h3 class="page-subheading">{l s='Already registered?'}</h3>
		<div class="form_content clearfix">
			<div class="form-group">
				<label for="email">{l s='Email address'}</label>
				<input class="is_required validate account_input form-control" data-validate="isEmail" type="email" id="email" name="email" value="{if isset($smarty.post.email)}{$smarty.post.email|stripslashes}{/if}" />
			</div>
			<div class="form-group">
				<label for="passwd">{l s='Password'}</label>
				<input class="is_required validate account_input form-control" type="password" data-validate="isPasswd" id="passwd" name="passwd" value="" />
			</div>
			<p class="lost_password form-group"><a href="{$link->getPageLink('password')|escape:'html':'UTF-8'}" title="{l s='Recover your forgotten password'}" rel="nofollow">{l s='Forgot your password?'}</a></p>
			<p class="submit">
				{if isset($back)}<input type="hidden" class="hidden" name="back" value="{$back|escape:'html':'UTF-8'}" />{/if}
				<button type="submit" id="SubmitLogin" name="SubmitLogin" class="button btn btn-default button-medium">
					<span>
						<i class="icon-lock left"></i>
						{l s='Sign in'}
					</span>
				</button>
			</p>
		</div>
	</form>
	{/if}
{/if}

-->

<!--
	code Serge
-->

	<div class="baniere">

		<div class="logo">
			<a href="{$base_dir}" title="Banniere SurLeChamp" >
				<img class="img-responsive " id="banner_logo" src="{$logo_img}" alt="logo surlechamp" title="logo SurLeCHamp"/>
			</a>
		</div>
		<div class="base-line">
			<img class="img-responsive " src="{$baseline_img}" alt="baseline surlechamp" title="baseline SurLeCHamp"/>
		</div>
		<div class="compte" >
		<p >Mon compte</p>
		{if logged}
			<div class="sous_compte">
				<a href="">Mon Compte</a>
				<a href="">Se deconnecter</a>
			</div>
		{/if}
		</div>
		

		<!--code pour se loguer-->



	{* si le client est logger, le formulaire n'est pas affiché *}

	{if !$logged}
		{if $page_name != 'authentication'}

		<form action="{$link->getPageLink('authentication', true)|escape:'html':'UTF-8'}" method="post" id="login_form" class="box">
			<fieldset>
				<legend><h3 class="page-subheading">{l s='Already registered?'}</h3></legend>
					
					<div class="lab1">
						<label for="email">{l s='Email address'}</label>
						<input class="is_required validate account_input form-control" placeholder="e-mail" data-validate="isEmail" type="email" id="email" name="email" value="{if isset($smarty.post.email)}{$smarty.post.email|stripslashes}{/if}" />	
					</div>
					
					<div class="lab2">
						<label for="passwd">{l s='Password'}</label>
						<input class="is_required validate account_input form-control" placeholder="Mot de passe" type="password" data-validate="isPasswd" id="passwd" name="passwd" value="" />	
					</div>
					

					<button type="submit" id="SubmitLogin" name="SubmitLogin" class="button">
						<span>
							<i class="icon-lock left"></i>
							{l s='Sign in'}
						</span>
					</button>
					<br>

					<p class="mp_oublie"><a href="{$link->getPageLink('password')|escape:'html':'UTF-8'}" title="{l s='Recover your forgotten password'}" rel="nofollow">{l s='Forgot your password?'}</a></p>
					<p class="submit">
					{if isset($back)}<input type="hidden" class="hidden" name="back" value="{$back|escape:'html':'UTF-8'}" />{/if}
			</fieldset>

		</form>
					{/if}
	{/if}


		<br>

		<nav>
					
				<ul>
				
					<li class="accueil"><a href="" ></a></li>
					<li class="qsn"><a href="">Qui sommes-nous?</a></li>
					<li class="vec"><a href="">Ventes en cours</a></li>
					<li class="destockage"><a href="">Destockage</a></li>
					<li class="produteurs"><a href="">Produteurs</a></li>
					<li class="recettes"><a href="">Idées recettes</a></li>
					<li class="panier" ><a href="http://localhost/prestashopslc/index.php?controller=order" title="Voir mon panier" rel="nofollow">

						<b><img src="" alt=""> Panier</b>
							<span class="ajax_cart_quantity unvisible">0</span>
							<span class="ajax_cart_product_txt unvisible">Produit</span>
							<span class="ajax_cart_product_txt_s unvisible">Produits</span>
							<span class="ajax_cart_total unvisible"></span>
							<span class="ajax_cart_no_product">(vide)</span>
						</a>
					</li>
					
				</ul>

		</nav>






	</div>


