<?php
/*
* 2015 LocalAddict
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Benoit MOTTIN <benaesan@msn.com>
*  @copyright  LocalAddict
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

if (!defined('_PS_VERSION_'))
	exit;

class slc_banner extends Module
{
	/**
	 * module's constructor / Required / grant to module to install himself
	 */
	public function __construct(){
		$this->name = 'slc_banner';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'Benoit MOTTIN';
		$this->need_instance = 0;
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
		$this->bootstrap = true;
	
		parent::__construct();
	
		$this->displayName = $this->l('SurLeChamp Banniere');
		$this->description = $this->l('Affiche la banniere de SurLeChamp');
	
		$this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
	}

	/**
	 * Show module's configuration
	 */
	public function getContent(){
		// //////////////TEST////////////////
	    
	}

    /**************************************************
     * 				INSTALLATION 					  *
     **************************************************/

	/**
	 * Execute certains traitement à l'installation du module
	 */
	public function install(){
			/**
			 * Installation du module parent
			 * enregistre au hook display header si besoin
			 * et s'enregistre au hook displaybanner si besoin
			 */
			if (!parent::install()
				|| !$this->registerHook('displayHeader') 
				|| !$this->registerHook('displayBanner')
				)
			{
				return false;
			}//*/
			$this->installFixture();
			return true;
		//}
	}

	/**
	 * Execute certains traitement à la désinstallation du module
	 */
	public function uninstall(){
		if (!parent::uninstall()){
				return false;
    		} 
			//*/
			//parent::uninstall();n
		return true;

    	}//*/ 

    /**
     * Affiche les truc dans le header
     * sert à mettre des liens pour le css et le js par exemple
     */
	public function hookDisplayHeader($params)	{
  	    $this->context->controller->addCSS($this->_path.'css/slcbanner.css', 'all');
	}   

	/**
	 * Affiche les elements en haut de la page
	 */
	public function hookDisplayTop($params){
		if (!$this->isCached('slc_banner.tpl', $this->getCacheId()))
		{
			// charge l'image de la banniere
			$imgbanner = "header.png";
			// VERIFIE si l'image existe et l'assigne a smarty pour l'afficher par la suite dans le template
			if ($imgbanner && file_exists(_PS_MODULE_DIR_.$this->name.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$imgbanner))
				$this->smarty->assign('banner_img', $this->context->link->protocol_content.Tools::getMediaServer($imgbanner).$this->_path.'img/'.$imgbanner);

			
			// french flag
			$imgFrenchFlag = "drapeau-francais.png";

			if ($imgFrenchFlag && file_exists(_PS_MODULE_DIR_.$this->name.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$imgFrenchFlag))
				$this->smarty->assign('french_flag_img', $this->context->link->protocol_content.Tools::getMediaServer($imgFrenchFlag).$this->_path.'img/'.$imgFrenchFlag);
			
			// english flag
			$imgEnglishFlag = "drapeau-anglais.png";

			if ($imgEnglishFlag && file_exists(_PS_MODULE_DIR_.$this->name.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$imgEnglishFlag))
				$this->smarty->assign('english_flag_img', $this->context->link->protocol_content.Tools::getMediaServer($imgEnglishFlag).$this->_path.'img/'.$imgEnglishFlag);
			
			// Logo
			$imglogo = "logo.png";
			if ($imglogo && file_exists(_PS_MODULE_DIR_.$this->name.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$imglogo))
				$this->smarty->assign('logo_img', $this->context->link->protocol_content.Tools::getMediaServer($imglogo).$this->_path.'img/'.$imglogo);

			// baseline
			$imgbaseline = "baseline.png";

			if ($imgbaseline && file_exists(_PS_MODULE_DIR_.$this->name.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$imgbaseline))
				$this->smarty->assign('baseline_img', $this->context->link->protocol_content.Tools::getMediaServer($imgbaseline).$this->_path.'img/'.$imgbaseline);

			// Smarty Assign 
			$this->smarty->assign(array(
				'banner_link' => Configuration::get('SLC_BANNER_LINK', $this->context->language->id),
				'banner_desc' => Configuration::get('SLC_BANNER_DESC', $this->context->language->id)
			));
		}

		return $this->display(__FILE__, 'slc_banner.tpl', $this->getCacheId());
	}

	public function hookDisplayBanner($params)
	{	// branche le module sur le hook Display Banner
		// /!\ Requis
		return $this->hookDisplayTop($params);
	}
//*/
	protected function installFixture($id_lang, $image = null)
	{	
		/**
		 * enregistrement des differentes info pour le fonctionnement du module
		 */
		$values['SLCBANNER_IMG'][(int)$id_lang] = $image;
		$values['SLCBANNER_LINK'][(int)$id_lang] = '';
		$values['SLCBANNER_DESC'][(int)$id_lang] = '';
		Configuration::updateValue('SLCBANNER_IMG', $values['SLCBANNER_IMG']);
		Configuration::updateValue('SLCBANNER_LINK', $values['SLCBANNER_LINK']);
		Configuration::updateValue('SLCBANNER_DESC', $values['SLCBANNER_DESC']);
	}
}